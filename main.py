#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser
from operator import mod
import torch
import numpy as np
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
# from trainer import model_evaluation
from trainer.module_visualization_mel import Model_visualization

from pytorch_lightning.callbacks import Callback
import sys
torch.multiprocessing.set_sharing_strategy('file_system')

def cli_main():
    # args
    parser = ArgumentParser()
    parser = Trainer.add_argparse_args(parser)
    parser = Model_visualization.add_model_specific_args(parser)
    args = parser.parse_args()
    np.random.seed(1)

    model = Model_visualization(**vars(args))

    if args.checkpoint_path is not None:
    # if args.visualization is None and args.classification is None:
        state_dict = torch.load(args.checkpoint_path, map_location="cpu")["state_dict"]
        # pop loss Function parameter
        loss_weights = []
        if args.keep_loss_weight is False or args.evaluate:
            for key, value in state_dict.items():
                if "loss" in key:
                    loss_weights.append(key)
            for item in loss_weights:
                state_dict.pop(item)
        model.load_state_dict(state_dict, strict=False)
        print("initial parameter from pretrain model {}".format(args.checkpoint_path))

    if args.verification is True :
        model.hparams.dev_list_path = args.dev_list_path
        model.cuda()
        model.eval()
        with torch.no_grad():
            model.cosine_evaluate()

    elif args.classification is True:
        print('Evaluate Classfication')
        model.hparams.dev_list_path = args.dev_list_path
        model.cuda()
        model.eval()
        with torch.no_grad():
            model.evaluate_classfication()
    
    elif args.visualization is True:
        model.hparams.dev_list_path = args.dev_list_path
        model.cuda()
        # print(model.speaker_encoder.layer4[2].conv2)
        speaker_classification_model = model.speaker_encoder.eval()
        model.evaluate_visualization(speaker_classification_model)

    if args.visualization_metrics is True:
        model.hparams.dev_list_path = args.dev_list_path
        model.cuda()
        # print(model.speaker_encoder.layer4[2].conv2)
        speaker_classification_model = model.speaker_encoder.eval()
        model.visualization_metrics(speaker_classification_model)
if __name__ == '__main__':  # pragma: no cover
    cli_main()

