# Reliable Visualization for Deep Speaker Recognition 
## Title
<b>Reliable Visualization for Deep Speaker Recognition[[PDF]](http://lipq.cslt.org/cam/paper.pdf) </b>

## Author
Pengqi Li, <a href="http://lilt.cslt.org/" >Lantian Li</a>, Askar Hamdulla, <a
                  href="http://wangd.cslt.org/" >Dong Wang</a>




## Abstract

In spite of the impressive success of convolutional neural networks (CNNs) in speaker recognition,
              our understanding to CNNs’ internal functions is still limited.
              A major obstacle is that some popular visualization tools are difficult to apply,
              for example those producing saliency maps.
              The reason is that speaker information does not show clear spatial patterns in the temporal-frequency
              space,
              which makes it hard to interpret the visualization results, and hence hard to confirm the reliability of a
              visualization tool.
              In this paper, we conduct an extensive analysis on three popular visualization methods based on CAM:
              Grad-CAM, Score-CAM and Layer-CAM, to investigate their reliability for speaker recognition tasks.
              Experiments conducted on a state-of-the-art ResNet34SE model show that the Layer-CAM algorithm can produce
              reliable visualization, and thus can be used as a promising tool to explain CNN-based speaker models. The
              source code and examples are available in our project page: [http://project.cslt.org/](http://project.cslt.org/) </a>

## Source

<a href="http://lipq.cslt.org/cam"> project page </a>

<a href="http://lipq.cslt.org/cam/paper.pdf"> paper </a>


## Dependencies

```
pip install -r requirements.txt
```
We used an open-source speaker recognition toolkit which is [sunine](https://gitlab.com/csltstu/sunine).

## Data
[Voxceleb2](https://www.robots.ox.ac.uk/~vgg/data/voxceleb/vox2.html).

## Model
Experiments conducted on ResNet34SE model.

Pre-trained models provided by [sunine](https://gitlab.com/csltstu/sunine)
+ Download

| Dataset  | BaiduYunDisk | AliYun |
|:---------|:------------ |:------ |
| VoxCeleb | [Here](https://pan.baidu.com/s/1Lp4C_c4rjISKUKYhMPT3_Q) (Code: tdwt) | [Here](http://cnsrc.cslt.org/download/ckpt/voxceleb-pretrain-model.tar.gz) |


## Example
```
cd egs/voxceleb/visualization
```
### Data preparation
We use single-speaker and multi-speaker utterances from Vox2.Dev training list in experiments. 

We provides lists of tests used in the experiment.
```
sh data_preprocess.sh
```

### Single-speaker experiment

In this section, we show the behavior of the three CAM algorithms using single-speaker utterances, i.e.,
            only a single target speaker exists in an utterance.

```
cd single
# Generate saliency maps
sh generate_salient.sh
# Evaluate performance
sh evaluate.sh
```

### Multi-speaker experiment

In this section, we show the behavior of the three CAM algorithms using Multi-speaker utterances. In the
            multi-speaker experiment, we concatenate an utterance of the target speaker with one or two utterances of
            other interfering speakers.

```
cd Multi
# Generate saliency maps
sh generate_salient.sh
# Evaluate performance
sh evaluate.sh
```


### Localization and recognition
We can use the saliency map as a tool to perform localization and recognition, i.e., ﬁrstly identify where the target speaker resides and then perform speaker recognition with the located segments only.

```
cd loc_rec/
# Generate saliency maps
sh generate_salient.sh
# fusion saliency maps 
sh fusion.sh
# Evaluate performance
sh evaluate.sh
```