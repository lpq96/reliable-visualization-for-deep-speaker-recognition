#!/usr/bin/env python
# encoding: utf-8

import os
from argparse import ArgumentParser
import numpy as np
import pandas as pd

import torch
# from torch import tensor
import torch.nn as nn
from pytorch_lightning import LightningModule, Trainer
import torch.nn.functional as F

from torch.utils.data import DataLoader

from pytorch_lightning.callbacks import ModelCheckpoint
import torchaudio
from tqdm import tqdm
import heapq

import importlib
from .metric import cosine_score, PLDA_score
from .metric.plda import PldaAnalyzer

from .utils import PreEmphasis
from .dataset_loader import Train_Dataset, Train_Sampler, Test_Dataset, Dev_Dataset, Test_classification_Dataset

from .cam.cam_give_gt import CAM, GradCAM, GradCAMpp, ScoreCAM, LayerCAM
from .cam import *
import matplotlib.pyplot as plt

class Model_visualization(LightningModule):
    def __init__(self, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        # load trials and data list
        if os.path.exists(self.hparams.trials_path):
            self.trials = np.loadtxt(self.hparams.trials_path, dtype=str)
        if os.path.exists(self.hparams.train_list_path):
            df = pd.read_csv(self.hparams.train_list_path)
            speaker = np.unique(df["utt_spk_int_labels"].values)
            self.hparams.num_classes = len(speaker)
            print("Number of Training Speaker classes is: {}".format(self.hparams.num_classes))
        self.hparams.threshold = float(self.hparams.threshold)/100
        # Network information Report
        print("Network Type: ", self.hparams.nnet_type)
        print("Pooling Type: ", self.hparams.pooling_type)
        print("Embedding Dim: ", self.hparams.embedding_dim)

        #########################
        ### Network Structure ###
        #########################

        if self.hparams.feature_type == 'mel-spectrum':
            self.mel_trans = torch.nn.Sequential(
                    PreEmphasis(),
                    torchaudio.transforms.MelSpectrogram(sample_rate=16000, n_fft=512, 
                        win_length=400, hop_length=160, window_fn=torch.hamming_window, n_mels=self.hparams.n_mels)
                    )
            self.instancenorm = nn.InstanceNorm1d(self.hparams.n_mels)

        melparam = { 'n_fft':512, 'win_length':400, 'hop_length':160, 'window_fn':torch.hamming_window, 'n_mels':self.hparams.n_mels}
        if self.hparams.feature_type == 'MFCC':
            self.mel_trans = torch.nn.Sequential(
                    PreEmphasis(),
                    torchaudio.transforms.MFCC(sample_rate=16000, n_mfcc=self.hparams.n_mfcc, melkwargs=melparam)
                    )
            self.instancenorm = nn.InstanceNorm1d(self.hparams.n_mfcc)

        # 2. Speaker_Encoder
        Speaker_Encoder = importlib.import_module('trainer.nnet.'+self.hparams.nnet_type).__getattribute__('Speaker_Encoder')
        self.speaker_encoder = Speaker_Encoder(**dict(self.hparams))

        # 3. Loss / Classifier
        if not self.hparams.evaluate:
            LossFunction = importlib.import_module('trainer.loss.'+self.hparams.loss_type).__getattribute__('LossFunction')
            self.loss = LossFunction(**dict(self.hparams))
        self.softmax = nn.Softmax()

    def forward(self, x, label):
        x = self.extract_speaker_embedding(x)
        x = x.reshape(-1, self.hparams.nPerSpeaker, self.hparams.embedding_dim)
        loss, acc = self.loss(x, label)
        return loss.mean(), acc

    def extract_speaker_embedding(self, data):
        x = data.reshape(-1, data.size()[-1])
        x = self.mel_trans(x) + 1e-6
        x = x.log() #! MFCC does not log
        x = self.instancenorm(x)
        x = self.speaker_encoder(x)
        return x

    
    def test_dataloader(self, trials):
        enroll_list = np.unique(trials.T[1])
        test_list = np.unique(trials.T[2])
        eval_list = np.unique(np.append(enroll_list, test_list))
        print("number of eval: ", len(eval_list))
        print("number of enroll: ", len(enroll_list))
        print("number of test: ", len(test_list))

        test_dataset = Test_Dataset(data_list=eval_list, eval_frames=self.hparams.eval_frames, num_eval=0)
        loader = DataLoader(test_dataset, num_workers=self.hparams.num_workers, batch_size=1)
        return loader
        
    def test_dataloader_for_classification(self):
        print(self.hparams.test_list_path)
        test_dataset = Test_classification_Dataset(
            data_list_path=self.hparams.test_list_path, eval_frames=self.hparams.eval_frames, num_eval=0)
        #! 为了测试同一张图片的可视化，暂时不shuffle
        loader = DataLoader(
            test_dataset, num_workers=self.hparams.num_workers, batch_size=1,shuffle=False)
        return loader
    
    def cosine_evaluate(self):
        eval_loader = self.test_dataloader(self.trials)
        index_mapping = {}
        eval_vectors = [[] for _ in range(len(eval_loader))]
        print("extract eval speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for idx, (data, label) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda()
                label = list(label)[0]
                index_mapping[label] = idx
                embedding = self.extract_speaker_embedding(data)
                embedding = torch.mean(embedding, axis=0)
                embedding = embedding.cpu().detach().numpy()
                eval_vectors[idx] = embedding
        eval_vectors = np.array(eval_vectors)
        print("scoring...")
        eer, th, mindcf_e, mindcf_h = cosine_score(self.trials, self.hparams.scores_path, index_mapping, eval_vectors)
        print("Cosine EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('cosine_eer', eer*100)
        self.log('minDCF(0.01)', mindcf_e)
        self.log('minDCF(0.001)', mindcf_h)
        return eer, th, mindcf_e, mindcf_h

    def evaluate(self):
        dev_dataset = Dev_Dataset(data_list_path=self.hparams.dev_list_path, eval_frames=self.hparams.eval_frames, num_eval=0)
        dev_loader = DataLoader(dev_dataset, num_workers=self.hparams.num_workers, batch_size=1)

        # first we extract dev speaker embedding
        dev_vectors = []
        dev_labels = []
        print("extract dev speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for data, label in tqdm(dev_loader):
                length = len(data)
                embedding = self.extract_speaker_embedding(data.cuda())
                embedding = embedding.reshape(length, 1, -1)
                embedding = torch.mean(embedding, axis=1)
                embedding = embedding.cpu().detach().numpy()
                dev_vectors.append(embedding)
                label = label.cpu().detach().numpy()
                dev_labels.append(label)

        dev_vectors = np.vstack(dev_vectors).reshape(-1, self.hparams.embedding_dim)
        dev_labels = np.hstack(dev_labels)
        print("dev vectors shape:", dev_vectors.shape)
        print("dev labels shape:", dev_labels.shape)

        eval_loader = self.test_dataloader(self.trials)
        index_mapping = {}
        eval_vectors = [[] for _ in range(len(eval_loader))]
        print("extract eval speaker embedding...")
        self.speaker_encoder.eval()
        with torch.no_grad():
            for idx, (data, label) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda()
                label = list(label)[0]
                index_mapping[label] = idx
                embedding = self.extract_speaker_embedding(data)
                embedding = torch.mean(embedding, axis=0)
                embedding = embedding.cpu().detach().numpy()
                eval_vectors[idx] = embedding

        eval_vectors = np.array(eval_vectors)
        print("eval_vectors shape is: ", eval_vectors.shape)

        print("scoring...")
        eer, th, mindcf_e, mindcf_h = cosine_score(self.trials, index_mapping, eval_vectors)
        print("Cosine EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('cosine eer', eer*100)
        self.log('cosine minDCF(0.01)', mindcf_e)
        self.log('cosine minDCF(0.001)', mindcf_h)

        # PLDA
        plda = PldaAnalyzer(n_components=self.hparams.plda_dim)
        plda.fit(dev_vectors, dev_labels, num_iter=10)
        eval_vectors_trans = plda.transform(eval_vectors)
        eer, th, mindcf_e, mindcf_h = PLDA_score(self.trials, index_mapping, eval_vectors_trans, plda)
        print("PLDA EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}".format(eer*100, mindcf_e, mindcf_h))
        self.log('plda eer', eer*100)
        self.log('plda minDCF(0.01)', mindcf_e)
        self.log('plda minDCF(0.001)', mindcf_h)

    def evaluate_classfication(self):
        eval_loader = self.test_dataloader_for_classification()
        eval_posterior = []
        predict_classes = []
        wav_path = []
        right_num = 0
        print("echo eval posteriori probability...")
        self.speaker_encoder.eval()
        self.loss.eval()
        with torch.no_grad():
            # for idx, (data, label) in enumerate(tqdm(eval_loader)):
            for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
                data = data.permute(1, 0, 2).cuda() # [1, 1, 81953] -> [1, 1, 81953] ref: evluate()
                label = label.cuda() # [1, 1, 81953] -> [1, 1, 81953] ref: evluate()

                #classification process
                logit = self.extract_speaker_embedding(data)
                logit = logit.unsqueeze(0)
                x = self.loss(logit)
                # x = self.softmax(logit) # logit.size() -> [1,400]
                x = x.cpu().numpy().tolist() # max(x[0]) is the max posterior
                label = label.cpu().numpy().tolist()[0]
                y_tilde = x[0].index(max(x[0]))
                # print("{},{}".format(y_tilde, label))
                if label == y_tilde :
                    right_num += 1
                    acc = (right_num / (idx+1)) * 100
                # Save the posterior corresponding to each wav into result.csv
                name = list(name)[0].split('/')[-1]
                wav_path.append(name)
                eval_posterior.append('{:.10f}'.format(x[0][label]))
                predict_classes.append(y_tilde)

        print("acc is {}%".format(acc))
        csv_dict = {"wav_path": wav_path,
                "eval_posterior": eval_posterior,
                "predict_classes": predict_classes,
                }
        df = pd.DataFrame(data=csv_dict)
        result_path  =  self.hparams.result_list_path
        try:
            df.to_csv(result_path)
            print(f'Saved data list file at {result_path}')
        except OSError as err:
            print(
                f'Ran in an error while saving {result_path}: {err}')

    def eval_forward(self,input,label,idx,topN):
        logit = self.speaker_encoder(input)
        logit = logit.unsqueeze(0)
        output = self.loss(logit,label)
        # output = self.softmax(output) # logit.size() -> [1,400]
        output = output.cpu().detach().numpy().tolist() # max(x[0]) is the max posterior
        label_int = label.cpu().numpy().tolist()[0]
        score = output[0][label]
        y_tilde = output[0].index(max(output[0]))
        topN_score = heapq.nlargest(topN,output[0])
        topN = heapq.nlargest(topN, range(len(output[0])), key=lambda x: output[0][x])

        return score, label_int, y_tilde, topN, topN_score

    def creat_importance_map(self,speaker_classification_model,input_mel_spectrogram,label):

        target_layers_name = [self.hparams.target_layer]
        
        target_layer = find_speaker_encoder_layer(speaker_classification_model,target_layers_name[0])
        if self.hparams.visualization_type == 'CAM':
          wrapped_model = CAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
          # cam, _ = wrapped_model(input_mel_spectrogram)
          cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
        if self.hparams.visualization_type == 'GradCAM':
          wrapped_model = GradCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
          cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
        if self.hparams.visualization_type == 'GradCAMpp':
          wrapped_model = GradCAMpp(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
          cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
        if self.hparams.visualization_type == 'ScoreCAM':
          wrapped_model = ScoreCAM(model=speaker_classification_model,loss=self.loss, target_layer=target_layer)
          cam, _ = wrapped_model(input_mel_spectrogram,idx=label)
        if self.hparams.visualization_type == 'LayerCAM':
          wrapped_model = LayerCAM(model=speaker_classification_model, loss=self.loss, target_layer=target_layer)
          cam, _ = wrapped_model(input_mel_spectrogram,idx=label)

        return cam


    def test_acc(self, input_size, input, explanation, type, threshold=0.15):
        H = input_size[0]
        W = input_size[1]
        start = torch.from_numpy(np.zeros(input.shape))[0]
        final = input.clone()[0].cpu().numpy()
        #! Coordinates of pixels in order of decreasing saliency (降序)
        # np.argsort 将input按照axis排序(升序)，并返回排序后的下标
        # np.flip  沿指定的轴反转数组元素的顺序，从而保留数组的形状(变成降序)
        if type == 'insertion':
          salient_order = np.flip(np.argsort(explanation.reshape(-1, H*W), axis=1), axis=-1)
        elif type == 'deletion':
          salient_order = np.argsort(explanation.reshape(-1, H*W), axis=1)
          threshold = 1.0 - threshold  #todo insertion 10% 相当于 deletion 倒序的 90%

        coords = salient_order[:, 0:int((H*W) * threshold)]
        start = start.cpu().numpy()
        start.reshape(1, H*W)[0,coords] = final.reshape(-1, H*W)[0,coords]
        start = torch.from_numpy(start.reshape(1, H, W))    

        return start.type(torch.FloatTensor).cuda()

    
    def random_test_acc(self, input_size, input, type, threshold=0.15):
        H = input_size[0]
        W = input_size[1]
        start  = torch.from_numpy(np.zeros(input.shape))[0]
        final = input.clone()[0].cpu().numpy()

        if type == 'insertion':
          random_order = np.arange(0,H*W)
          np.random.shuffle(random_order)
        elif type == 'deletion':
          random_order = np.arange(0,H*W)
          np.random.shuffle(random_order)
          threshold = 1.0 - threshold  #todo insertion 10% 相当于 deletion 倒序的 90%

        # random_order = np.arange(0,H*W)
        # np.random.shuffle(random_order)
        coords = random_order[0:int((H*W) * threshold)]
        start = start.cpu().numpy()
        start.reshape(1, H*W)[0,coords] = final.reshape(-1, H*W)[0,coords]
        start = torch.from_numpy(start.reshape(1, H, W))    
        
        return start.type(torch.FloatTensor).cuda()


    def occlusion_random_test_acc(self, input_size, input, type, threshold=0.15):
        H = input_size[0]
        W = input_size[1]
        final  = torch.from_numpy(np.zeros(input.shape))[0]
        start = input.clone()[0].cpu().numpy()

        random_order = np.arange(0,H*W)
        np.random.shuffle(random_order)
        coords = random_order[0:int((H*W) * threshold)]
        final = final.cpu().numpy()
        start.reshape(1, H*W)[0,coords] = final.reshape(-1, H*W)[0,coords]
        start = torch.from_numpy(start.reshape(1, H, W))    
       
        return start.type(torch.FloatTensor).cuda()

    def deletion_time_test_acc(self, input_size, input, type, threshold=0.15):
        H = input_size[0]
        W = input_size[1]
        final  = torch.from_numpy(np.zeros(input.shape))[0]
        start = input.clone()[0].cpu().numpy().reshape(1, H*W, order='F')
        final = final.cpu().numpy()
        start[0, 0:int((H*W) * threshold)] = final.reshape(-1, H*W, order='F')[0, 0:int((H*W) * threshold)]
        start = torch.from_numpy(start.reshape(1, H, W, order='F'))    
        
        return start.type(torch.FloatTensor).cuda()

    def insertion_time_test_acc(self, input_size, input, type, threshold=0.15):
        H = input_size[0]
        W = input_size[1]
        start  = torch.from_numpy(np.zeros(input.shape))[0]
        final = input.clone()[0].cpu().numpy().reshape(1, H*W, order='F')
        
        start = start.cpu().numpy().reshape(1, H*W, order='F')
        start[0, 0:int((H*W) * threshold)] = final[0, 0:int((H*W) * threshold)]
        start = torch.from_numpy(start.reshape(1, H, W, order='F'))    
        
        return start.type(torch.FloatTensor).cuda()



    def evaluate_visualization(self,speaker_classification_model):
        eval_loader = self.test_dataloader_for_classification()
        heat_map_cow_list = [] 
        feature_map_cow_list = []
        i = 0

        # 初始化模型；初始化可视化方法
        self.speaker_encoder.eval()
        self.loss.eval()
        os.system('mkdir -p {}/{}'.format(self.hparams.vis_result,self.hparams.visualization_type))
        os.system('mkdir -p {}/input'.format(self.hparams.vis_result))

        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()#! MFCC 不能提取log
            #todo test and draw mel-spectrum for input
            input_mel_spectrogram = self.instancenorm(x)
            input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(input_mel_spectrogram,label,idx,self.hparams.topN)
            if self.hparams.use_label == 'use_label':
              cam_merge = self.creat_importance_map(speaker_classification_model,input_mel_spectrogram,label)
            if self.hparams.use_label == 'use_pred':
              cam_merge = self.creat_importance_map(speaker_classification_model,input_mel_spectrogram,y_tilde)

            if self.hparams.save_np_pic:
              heat_map_cow = cam_merge.cpu().numpy()[0,0,:,:].ravel()
              heat_map_cow_list.append(heat_map_cow.ravel())
              if len(heat_map_cow_list) == 50 or len(eval_loader) == idx+1:
                np.save('{}/{}/{}_pic_{}.npy'.format(self.hparams.vis_result,self.hparams.visualization_type,self.hparams.visualization_type, i), heat_map_cow_list)
                heat_map_cow_list = []
                i=i+1

            if self.hparams.input_save_np_pic or len(eval_loader) == idx+1:
              feature_map_cow = input_mel_spectrogram.cpu().numpy()[0,:,:].ravel()
              feature_map_cow_list.append(feature_map_cow.ravel())
              if len(feature_map_cow_list) == 50:
                np.save('{}/input/input_pic_{}.npy'.format(self.hparams.vis_result, i), feature_map_cow_list)
                feature_map_cow_list = []
                i=i+1



    def scale(self,x,scale_gama):
        return np.tanh(scale_gama * x / x.max())

    def np2fig(self, label, input_mel_spectrogram, cam, idx):
        # save fig class activation map
        label_int = label.cpu().numpy().tolist()[0]
        heat_map("{}_{};\nlabel:{}".format(self.hparams.visualization_type, self.hparams.target_layer, label_int), \
        input_mel_spectrogram.cpu(), cam.cpu().numpy(), \
        save_path='{}/save_pic/{}_heat_map_{}_{}_scale_{}.png'.format(self.hparams.vis_result,idx,self.hparams.visualization_type,self.hparams.target_layer,self.hparams.scale_gama))
        plt.figure()
        plt.imshow(input_mel_spectrogram[0,:,:].cpu().numpy(), cmap='jet')
        plt.savefig('{}/input/{}_input_mel_spectrogram.png'.format(self.hparams.vis_result,idx), bbox_inches='tight')

    def repocessing(self,cam, input_size):
        cam.reshape(input_size[0], input_size[1])
        cam_time = np.sum(cam.reshape(input_size[0], input_size[1]),axis=0)
        cam_min, cam_max = cam_time.min(), cam_time.max()
        # todo !!!!
        repocessed_arr = np.tile(((cam_time - cam_min)/(cam_max - cam_min + 1e-8)), (80,1))
        repocessed_arr = repocessed_arr.reshape(-1,1,input_size[0], input_size[1])
        print(repocessed_arr.shape)
        w_mask = np.int64((repocessed_arr-0.6) > 0)
        # threshold is set 0.6
        heat_map("title", [], w_mask, save_path='test.png')
        import ipdb;ipdb.set_trace()
        return w_mask

    def visualization_metrics(self,speaker_classification_model):
        eval_loader = self.test_dataloader_for_classification()
        insertion_h_list, deletion_h_list = [],[]
        importance_acc_list, origin_acc_list = [],[]
        i = 0

        # 初始化模型；初始化可视化方法
        self.speaker_encoder.eval()
        self.loss.eval()
        os.system('mkdir -p {}/{}'.format(self.hparams.vis_result,self.hparams.visualization_type))
        os.system('mkdir -p {}/input'.format(self.hparams.vis_result))
        os.system('mkdir -p {}/save_pic'.format(self.hparams.vis_result))

        f1 = open('{}/{}_origin_scores_{}_{}.csv'.format(self.hparams.vis_result,self.hparams.visualization_type,self.hparams.test_type,self.hparams.threshold),'w')
        f2 = open('{}/{}_salient_scores_{}_{}.csv'.format(self.hparams.vis_result,self.hparams.visualization_type,self.hparams.test_type,self.hparams.threshold),'w')
        

        for idx, (data, label, name) in enumerate(tqdm(eval_loader)):
            data = data.permute(1, 0, 2).cuda()
            label = label.cuda()
            # todo data preprocess
            x = data.reshape(-1, data.size()[-1])
            x = self.mel_trans(x) + 1e-6
            x = x.log()
            input_mel_spectrogram = self.instancenorm(x)
            cams_idx = idx % 50
            input_size = input_mel_spectrogram.squeeze(0).shape
            if self.hparams.visualization_type != "_":
              if cams_idx == 0:
                cams = np.load('{}/{}/{}_pic_{}.npy'.format(self.hparams.vis_result,self.hparams.visualization_type,self.hparams.visualization_type, i), allow_pickle=True)
                i = i+1
              if self.hparams.scale_gama != 0:
                cam_np = self.scale(cams[cams_idx], self.hparams.scale_gama)
              else:
                cam_np = cams[cams_idx]
              self.repocessing(cam_np,input_size)
              cam = torch.from_numpy(cam_np.reshape(-1,1, input_size[0], input_size[1])).cuda()

            if self.hparams.save_pic:
              self.np2fig(label, input_mel_spectrogram, cam, idx)
              

            #todo save top-50 scores for compute top-N acc
            if 'insertion' in self.hparams.test_type or 'deletion' in self.hparams.test_type or 'multiplication' in self.hparams.test_type:
              input_target_class_posterior, label_int, y_tilde, topN, topN_score = self.eval_forward(input_mel_spectrogram,label,idx,self.hparams.topN)
              f1.write('{},{},{},{},{}\n'.format(input_target_class_posterior,y_tilde,label, topN, topN_score))
              if 'save_acc_multiplication' == self.hparams.test_type:
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(input_mel_spectrogram*cam[0,0,:,:],label,idx,self.hparams.topN)

              if 'save_acc_deletion' == self.hparams.test_type:
                cam = cam[0].cpu().numpy()
                importance_local=self.test_acc(input_size,input_mel_spectrogram, cam, 'deletion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)

              if 'save_acc_insertion' == self.hparams.test_type:
                cam = cam[0].cpu().numpy()
                importance_local=self.test_acc(input_size,input_mel_spectrogram, cam, 'insertion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)

              if 'save_acc_random_deletion' == self.hparams.test_type:
                importance_local=self.random_test_acc(input_size,input_mel_spectrogram, 'deletion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)
              
              if 'save_acc_random_insertion' == self.hparams.test_type:
                importance_local=self.random_test_acc(input_size,input_mel_spectrogram, 'insertion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)

              if 'save_acc_time_deletion' == self.hparams.test_type:
                importance_local=self.deletion_time_test_acc(input_size,input_mel_spectrogram, 'deletion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)
              
              if 'save_acc_time_insertion' == self.hparams.test_type:
                importance_local=self.insertion_time_test_acc(input_size,input_mel_spectrogram, 'insertion', threshold=self.hparams.threshold)
                input_target_class_posterior, label_int, y_tilde, topN, topN_score  = self.eval_forward(importance_local,label,idx,self.hparams.topN)

              f2.write('{},{},{},{},{}\n'.format(input_target_class_posterior,y_tilde,label, topN, topN_score))


    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)

        # Data Loader       
        parser.add_argument('--max_frames', type=int, default=201)
        parser.add_argument('--min_frames', type=int, default=200)
        parser.add_argument('--eval_frames', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=32)
        parser.add_argument('--max_seg_per_spk', type=int, default=500, help='Maximum number of utterances per speaker per epoch')
        parser.add_argument('--nPerSpeaker', type=int, default=1, help='Number of utterances per speaker per batch, only for metric learning based losses');
        parser.add_argument('--num_workers', type=int, default=16)
        parser.add_argument('--augment', action='store_true', default=False)

        # Training details
        parser.add_argument('--feature_type', type=str, default="mel-spectrum")
        parser.add_argument('--max_epoch', type=int, default=500, help='Maximum number of epochs')
        parser.add_argument('--loss_type', type=str, default="softmax")
        parser.add_argument('--nnet_type', type=str, default="ResNetSE34L")
        parser.add_argument('--pooling_type', type=str, default="SAP")
        parser.add_argument('--eval_interval', type=int, default=1)
        parser.add_argument('--keep_loss_weight', action='store_true', default=False)

        # Optimizer
        parser.add_argument('--learning_rate', type=float, default=0.001)
        parser.add_argument('--lr_step_size', type=int, default=10)
        parser.add_argument('--lr_gamma', type=float, default=0.95)

        # Loss functions
        parser.add_argument('--margin', type=float, default=0.2)
        parser.add_argument('--scale', type=float, default=30)

        # Training and test data
        parser.add_argument('--train_list_path', type=str, default='')
        parser.add_argument('--dev_list_path', type=str, default='')
        parser.add_argument('--test_list_path', type=str, default='')
        parser.add_argument('--trials_path', type=str, default='trials.lst')
        parser.add_argument('--scores_path', type=str, default='scores.lst')
        parser.add_argument('--musan_list_path', type=str, default='')
        parser.add_argument('--rirs_list_path', type=str, default='')
        parser.add_argument('--result_list_path', type=str, default='')

        # Load and save
        parser.add_argument('--checkpoint_path', type=str, default=None)
        parser.add_argument('--save_top_k', type=int, default=15)
        parser.add_argument('--suffix', type=str, default='')

        # Model definition
        parser.add_argument('--n_mels', type=int, default=80)
        parser.add_argument('--n_mfcc', type=int, default=40)
        parser.add_argument('--embedding_dim', type=int, default=256)
        parser.add_argument('--plda_dim', type=int, default=256)

        # Test mode
        parser.add_argument('--evaluate', action='store_true', default=False)
        parser.add_argument('--classification', action='store_true', default=False)
        parser.add_argument('--verification', action='store_true', default=False)

        #visualization
        parser.add_argument('--visualization', action='store_true', default=False)
        parser.add_argument('--visualization_metrics', action='store_true', default=False)
        parser.add_argument('--visualization_type', type=str, default='CAM')
        parser.add_argument('--target_layer', type=str, default='layer4')
        parser.add_argument('--vis_result', type=str, default='visualization result dir')
        parser.add_argument('--topN', type=int, default=10)
        parser.add_argument('--save_pic', action='store_true', default=False)
        parser.add_argument('--save_np_pic', action='store_true', default=False)
        parser.add_argument('--input_save_np_pic', action='store_true', default=False)
        parser.add_argument('--save_auc', action='store_true', default=False)
        parser.add_argument('--test_type', type=str, help='save_acc_multiplication,save_acc_deletion,save_acc_insertion,save_acc_random_deletion,save_acc_random_insertion,save_acc_time_deletion,save_acc_time_insertion',default='')
        parser.add_argument('--use_label', type=str, help='use_label,use_pred',default='use_label')

        parser.add_argument('--scale_gama', type=int, default=0)
        parser.add_argument('--threshold', type=int, default=0) #todo input int  convert float as threshold

        return parser

