#!/bin/bash
stage=3
data_vad=/nfs/database/VoxCeleb/voxceleb2/dev/aac
SPEAKER_TRAINER_ROOT=../../..
if [ $stage -le 1 ];then
   # employ vad for single dataset
   python local/creat_vad_list.py \
           --wav_list data/data/single.csv \
           --vad_list data/data/single_vad.csv
   # step/compute_vad.py(set_mode is 3) do vad for local dir 
   echo compute vad $data_vad
   python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
           --data_dir $data_vad \
           --extension wav \
           --speaker_level 1 \
           --num_jobs 40
fi

if [ $stage -le 2 ];then
  python local/comb_from_source.py --data ./data
fi

if [ $stage -le 3 ];then
  python local/comb_noise.py --data ./data \
                             --noise_dir /work100/lilt/database/musan/noise
fi