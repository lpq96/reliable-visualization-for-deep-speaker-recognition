#!/bin/bash

SPEAKER_TRAINER_ROOT=../../../..

pooling_type=TSP
embedding_dim=256
scale=30.0
margin=0.2
cuda_device=2,3
nnet_type=ResNet34  #ResNet34_half ResNet34_sim
feature_type=mel-spectrum
ckpt_path=../ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt
loss_type=aamsoftmax_eval #stand_softmax norm_softmax aamsoftmax_only_margin aamsoftmax_margin+scale amsoftmax_only_margin amsoftmax_only_margin+scale

stage=0

if [ $stage -eq 0 ];then
  #todo save calss activation maps for single-speaker
  # test_list_path=../data/data/test_noise.csv  # find out class activation maps for this wav
  test_list_path=../data/data/multi_a.csv  # find out class activation maps for this wav
  train_list_path=../data/train_lst.csv
  #! check save_path
  #! check target_layer
  target_layer=${layer} #layer1-4

  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --classification \
          --result_list_path result.csv \
          --keep_loss_weight \
          --save_np_pic \
          --feature_type $feature_type \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --train_list_path $train_list_path \
          --test_list_path $test_list_path \
          --nnet_type $nnet_type \
          --loss_type $loss_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 2
fi
