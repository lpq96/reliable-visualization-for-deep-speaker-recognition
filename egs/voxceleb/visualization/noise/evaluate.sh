#!/bin/bash

SPEAKER_TRAINER_ROOT=../../../..

pooling_type=TSP
embedding_dim=256
scale=30.0
margin=0.2
nnet_type=ResNet34  
feature_type=mel-spectrum
ckpt_path=../ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt
loss_type=aamsoftmax_eval 

stage=3
cuda_device=1


if [ $stage -eq 0 ];then
  #todo Statistically Evaluate the reliability of the class activation maps
  #todo this stage for evaluate single-speaker exp by auc
  test_list_path=../data/data/test_noise.csv  # find out class activation maps for this wav
  train_list_path=../data/train_lst.csv
  # test_type is 6 curves
  for test_type in  save_acc_deletion save_acc_insertion 
  do
  for visualization_type in GradCAMpp  LayerCAM   
  do
  for layer in layer4 
  do
    for threshold in  0 10 20 30 40 50 60 70 80 90 100
    do
    vis_result=salient_np_vad/$visualization_type/$layer
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer}
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result
    echo threshold $threshold
    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --visualization_metrics \
            --keep_loss_weight \
            --test_type $test_type \
            --threshold ${threshold} \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
    done
  done
  done
fi


if [ $stage -le 1 ];then
  test_list_path=../data/data/test_noise.csv  # find out class activation maps for this wav
  train_list_path=../data/train_lst.csv
  # test_type is 4 curves (baseline)
  for test_type in  save_acc_random_deletion save_acc_random_insertion save_acc_time_deletion save_acc_time_insertion
  do
  for visualization_type in _ 
  do
  for layer in layer4 
  do
    for threshold in 0 10 20 30 40 50 60 70 80 90 100
    do
    vis_result=salient_np_vad/baseline
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer} #layer1-4;  layer_merge
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result
    echo threshold $threshold
    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --visualization_metrics \
            --keep_loss_weight \
            --test_type $test_type \
            --threshold ${threshold} \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
    done
  done
  done
fi


if [ $stage -le 2 ];then
  # compute_acc according to the results above.
  sh local/compute_acc.sh 1
  # draw auc curve
  python local/auc_single.py \
          --deletion_file  ./result/deletion_vad_single_speaker.log \
          --insertion_file  ./result/insertion_vad_single_speaker.log \
          --deletion_time_align  ./result/deletion_time_vad_single_speaker.log \
          --insertion_time_align  ./result/insertion_time_vad_single_speaker.log \
          --deletion_random  ./result/deletion_random_vad_single_speaker.log \
          --insertion_random  ./result/insertion_random_vad_single_speaker.log
fi


if [ $stage -le 3 ];then
  test_list_path=../data/data/test_noise.csv  # find out class activation maps for this wav
  train_list_path=../data/train_lst.csv
  # test_type is 4 curves (baseline)
  for visualization_type in LayerCAM  
  do
  for layer in layer4 
  do
    vis_result=salient_np_vad/$visualization_type/$layer
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer} #layer1-4;  layer_merge
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result
    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --visualization_metrics \
            --keep_loss_weight \
            --save_pic \
            --scale_gama 5 \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
  done
fi