#!/bin/bash
stage=$1
mkdir -p result

###########   multi-speaekr exp  ###############

if [ $stage -eq 3 ];then

result=result/multiplication_multi_speaker.log
time=$(date "+%Y-%m-%d %H:%M:%S")
echo $time > $result

for visualization_type in GradCAMpp LayerCAM ScoreCAM
  do
  for layer in layer1 layer2 layer3 layer4 
  do
    for part in  a_b a_b_c b_a_b a_b_a
    do
  echo $visualization_type,$layer >> $result
    python3 local/pic_posterior_acc.py \
    --feature  salient_np/$part/${visualization_type}/$layer/${visualization_type}_origin_scores_save_acc_multiplication_0.0.csv\
    --saliency  salient_np/$part/${visualization_type}/$layer/${visualization_type}_salient_scores_save_acc_multiplication_0.0.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
done
done

for visualization_type in GradCAMpp LayerCAM ScoreCAM
  do
  for layer in  layer4_3_2_1 layer4_3_2 layer4_3
  do
  for part in a_b a_b_c b_a_b a_b_a
  do
  echo $visualization_type,$layer >> $result
    python3 local/pic_posterior_acc.py \
    --feature  salient_np/merge_scale5_add_avg/$visualization_type/$part/$layer/${visualization_type}_origin_scores_save_acc_multiplication_0.0.csv\
    --saliency  salient_np/merge_scale5_add_avg/$visualization_type/$part/$layer/${visualization_type}_salient_scores_save_acc_multiplication_0.0.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
done
done
fi
