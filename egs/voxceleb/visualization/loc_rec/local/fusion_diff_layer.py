#!/usr/bin/env python
# encoding: utf-8
import numpy as np
import argparse
import os
import sys
def fusion(layer1_dir, layer2_dir, layer3_dir, layer4_dir, layer_fusion_dir,scale):
    visualization_type = os.path.basename(layer1_dir)
    option = os.path.basename(layer_fusion_dir)
    npy_list = os.listdir(layer1_dir)
    os.system('mkdir -p {}/{}'.format(layer_fusion_dir, visualization_type))
    for file_idx in range(len(npy_list[:])):
      fusion_list = []
      file_name = '{}_pic_{}.npy'.format(visualization_type,file_idx)

      layer1_file = os.path.join(layer1_dir,file_name)
      layer2_file = os.path.join(layer2_dir,file_name)
      layer3_file = os.path.join(layer3_dir,file_name)
      layer4_file = os.path.join(layer4_dir,file_name)
      layer1_np = np.load(layer1_file,allow_pickle=True)
      layer2_np = np.load(layer2_file,allow_pickle=True)
      layer3_np = np.load(layer3_file,allow_pickle=True)
      layer4_np = np.load(layer4_file,allow_pickle=True)
      for i in range(len(layer1_np)):
        if option == 'layer4_3_2_1':
          stack = np.stack((layer1_np[i],layer2_np[i],layer3_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_3_2':
          stack = np.stack((layer2_np[i],layer3_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_2_1':
          stack = np.stack((layer1_np[i],layer2_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_3_1':
          stack = np.stack((layer1_np[i],layer3_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_3':
          stack = np.stack((layer3_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_2':
          stack = np.stack((layer2_np[i],layer4_np[i]),axis=0)
        elif option == 'layer4_1':
          stack = np.stack((layer1_np[i],layer4_np[i]),axis=0)
        else:
          print("layer_fusion_dir name must be layer4_3_2_1 or layer4_3_2 or layer4_3")
          sys.exit()
        num_layer = len(option.split('_'))
        #todo maximum opreation   ! dont't work!~
        # fusion = np.amax(stack, axis=0)
        #todo sum different layer cam    ! dont't work!~
        # fusion = np.sum(stack, axis=0)
        # fusion = np.tanh(fusion) #经过实验 torch.tanh 和np.tanh 效果一样
        #todo sum different layer cam 
        stack = np.tanh(scale * stack / stack.max())
        fusion = np.sum(stack, axis=0)
        fusion = fusion/num_layer
        #todo sum different layer cam and max/min norm     ! dont't work!~
        # fusion = np.sum(stack, axis=0)
        # cam_min, cam_max = fusion.min(), fusion.max()
        # fusion = (fusion - cam_min)/(cam_max - cam_min + 1e-8)
        fusion_list.append(fusion)
      np.save('{}/{}/{}_pic_{}.npy'.format(layer_fusion_dir, visualization_type, visualization_type, file_idx), fusion_list)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--layer1_dir', help='layer1 dir', type=str, default="")
    parser.add_argument('--layer2_dir', help='layer2 dir', type=str, default="")
    parser.add_argument('--layer3_dir', help='layer3 dir', type=str, default="")
    parser.add_argument('--layer4_dir', help='layer4 dir', type=str, default="")
    parser.add_argument('--layer_fusion_dir', help='layer4 dir', type=str, default="")

    parser.add_argument('--option', help='layer4_3_2_1', type=str, default="")
    parser.add_argument('--scale', help='gama', type=int, default="")
    
    args = parser.parse_args()
    fusion(args.layer1_dir, args.layer2_dir, args.layer3_dir, args.layer4_dir, args.layer_fusion_dir, args.scale)