#!/bin/bash

SPEAKER_TRAINER_ROOT=../../../..

pooling_type=TSP
embedding_dim=256
scale=30.0
margin=0.2
cuda_device=2
nnet_type=ResNet34  #ResNet34_half ResNet34_sim
feature_type=mel-spectrum
ckpt_path=../ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt
loss_type=aamsoftmax_eval #stand_softmax norm_softmax aamsoftmax_only_margin aamsoftmax_margin+scale amsoftmax_only_margin amsoftmax_only_margin+scale
train_list_path=../data/train_lst.csv

stage=0

if [ $stage -eq 0 ];then
  #todo Statistically Evaluate the reliability of the class activation maps
  #todo this stage for evaluate multi-speaker exp by Explanation map  about  layer4 layer3 layer2 layer1(scaled) 
  scale_gama=5
  for visualization_type in GradCAMpp ScoreCAM LayerCAM 
  do
  for layer in layer4 layer3 layer2 layer1 
  do
    for part in a_b a_b_c b_a_b a_b_a
    do 
    test_list_path=../data/data/test_${part}.csv  # find out class activation maps for this wav
    vis_result=salient_np/$part/$visualization_type/$layer
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer} #layer1-4;  layer_merge
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result
    echo scale_gama $scale_gama

    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main_mel.py \
            --visualization_metrics \
            --keep_loss_weight \
            --test_type save_acc_multiplication \
            --scale_gama $scale_gama \
            --auc_step 100 \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
  done
  done
fi


if [ $stage -eq 1 ];then
  #todo Statistically Evaluate the reliability of the class activation maps
  #todo this stage for evaluate multi-speaker exp by Explanation map about layer4_3_2_1 layer4_3_2 layer4_3 
  for visualization_type in GradCAMpp ScoreCAM LayerCAM
  do
  for layer in layer4_3_2_1 layer4_3_2 layer4_3   
  do
  for part in a_b a_b_c b_a_b a_b_a
    do
    test_list_path=../data/data/test_${part}.csv  # find out class activation maps for this wav
    vis_result=salient_np/merge_scale5_add_avg/${visualization_type}/${part}/${layer}
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer} #layer1-4;  layer_merge
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result

    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main_mel.py \
            --visualization_metrics \
            --keep_loss_weight \
            --test_type save_acc_multiplication \
            --auc_step 100 \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
  done
  done
fi


if [ $stage -eq 2 ];then
  # compute_acc according to the results above.
  sh local/compute_acc.sh 3
  
fi
