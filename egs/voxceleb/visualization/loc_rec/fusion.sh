#!/bin/bash

stage=0
if [ $stage -eq 0 ];then
  #todo fusion layer4_3_2_1 layer4_3_2 layer4_3 by layer4-1
  scale_gama=5
  for visualization_type in GradCAMpp ScoreCAM LayerCAM
  do
  {
  for fusion_layer in layer4_3_2_1 layer4_3_2 layer4_3   
  do
  {
  for part in a_b a_b_c b_a_b a_b_a
    do
    {
    echo $layer $visualization_type 
    python local/fusion_diff_layer.py \
          --layer1_dir salient_np/$part/$visualization_type/layer1/$visualization_type \
          --layer2_dir salient_np/$part/$visualization_type/layer2/$visualization_type \
          --layer3_dir salient_np/$part/$visualization_type/layer3/$visualization_type \
          --layer4_dir salient_np/$part/$visualization_type/layer4/$visualization_type \
          --layer_fusion_dir salient_np/merge_scale${scale_gama}_add_avg/${visualization_type}/${part}/${fusion_layer} \
          --scale $scale_gama
    }&
  done
  wait
  }&
  done
  wait
  }&
  done
  wait
fi
