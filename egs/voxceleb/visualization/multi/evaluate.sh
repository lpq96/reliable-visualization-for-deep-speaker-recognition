#!/bin/bash

SPEAKER_TRAINER_ROOT=../../../..

pooling_type=TSP
embedding_dim=256
scale=30.0
margin=0.2
cuda_device=0
nnet_type=ResNet34  #ResNet34_half ResNet34_sim
feature_type=mel-spectrum
ckpt_path=../ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt
loss_type=aamsoftmax_eval #stand_softmax norm_softmax aamsoftmax_only_margin aamsoftmax_margin+scale amsoftmax_only_margin amsoftmax_only_margin+scale
train_list_path=../data/train_lst.csv

stage=0

if [ $stage -eq 0 ];then
  #todo Statistically Evaluate the reliability of the class activation maps
  #todo this stage for evaluate multi-speaker exp by auc
  test_list_path=../data/data/test_b_a_b.csv  # find out class activation maps for this wav 
  # test_type is 6 curves
  for test_type in save_acc_deletion save_acc_insertion
  do
  for visualization_type in  GradCAMpp ScoreCAM LayerCAM 
  do
  for layer in layer4
  do
    for threshold in 0 10 20 30 40 50 60 70 80 90 100
    do
    vis_result=salient_np/b_a_b/$visualization_type/$layer
    mkdir -p $vis_result
    #! check target_layer
    target_layer=${layer} #layer1-4;  layer_merge
    echo visualization_type $visualization_type
    echo target_layer $target_layer
    echo save_path $vis_result
    echo threshold $threshold
    # save_pic; save_acc_preserve save_acc_occlusion save_acc_multiplication threshold scale_gama scale_delta;  save_auc
    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main_mel.py \
            --visualization_metrics \
            --keep_loss_weight \
            --test_type $test_type \
            --threshold ${threshold} \
            --auc_step 100 \
            --visualization_type $visualization_type \
            --feature_type $feature_type \
            --target_layer $target_layer \
            --vis_result $vis_result \
            --checkpoint_path $ckpt_path \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --nnet_type $nnet_type \
            --loss_type $loss_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 15 \
            --gpus 1
    done
    done
  done
  done
fi

if [ $stage -eq 1 ];then
  # compute_acc according to the results above.
  sh local/compute_acc.sh 2 
  python local/auc_multi.py \
        --deletion_file  ./result/deletion_multi_speaker.log \
        --insertion_file  ./result/insertion_multi_speaker.log
fi
