#!/bin/bash
stage=$1
mkdir -p result

###########   multi-speaekr exp  ###############

if [ $stage -eq 2 ];then
  # ! exp3 final
for type in insertion deletion 
do
result=result/${type}_multi_speaker.log
time=$(date "+%Y-%m-%d %H:%M:%S")
echo $time > $result
for visualization_type in GradCAMpp ScoreCAM LayerCAM 
  do
  for layer in layer4
  do
  echo $visualization_type,$layer >> $result
  for threshold in 0 10 20 30 40 50 60 70 80 90 100
  do
    i=`python3 local/compute_float.py --threshold $threshold`
    python3 local/pic_posterior_acc.py \
    --feature  salient_np/b_a_b/$visualization_type/layer4/${visualization_type}_origin_scores_save_acc_${type}_${i}.csv \
    --saliency  salient_np/b_a_b/$visualization_type/layer4/${visualization_type}_salient_scores_save_acc_${type}_${i}.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
  done
  done
done
fi
