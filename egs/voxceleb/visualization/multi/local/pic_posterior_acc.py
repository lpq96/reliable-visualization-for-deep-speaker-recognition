#!/usr/bin/env python
# encoding: utf-8
import numpy as np
import argparse
from sklearn.metrics.pairwise import cosine_similarity
import os
import matplotlib.pyplot as plt
import seaborn as sns

def pic_posterior(origin, importance, option):
    
    origin_file = open(origin, "r")
    origin_list = origin_file.readlines()
    importance_file = open(importance, "r")
    importance_list = importance_file.readlines()
    origin_posterior = []
    importance_posterior = []
    topN_num_origin, topN_num_importance, = 0,0

    # N=10
    N = int(option.split('-')[-1])

    for i in range(len(origin_list)):
      origin_posterior.append(float(origin_list[i].split(',')[0]))
      importance_posterior.append(float(importance_list[i].split(',')[0]))

      ori_y_label = origin_list[i].split('[')[1].split(']')[0]
         
      topN_ori = origin_list[i].split('[')[2].split(']')[0].split(', ')[:N]# 注意这后面有个空格
      if ori_y_label in topN_ori:
        topN_num_origin = topN_num_origin + 1

      importance_y_label = importance_list[i].split('[')[1].split(']')[0]
      topN_importance = importance_list[i].split('[')[2].split(']')[0].split(', ')[:N]# 注意这后面有个空格
      if importance_y_label in topN_importance:
        topN_num_importance = topN_num_importance + 1
        
    topN_acc_origin = topN_num_origin/(len(origin_list))   

    topN_acc_importance = topN_num_importance/(len(origin_list)) 
    # print('origin: top-1:{},{}:{}'.format(acc_origin,option,topN_acc_origin))
    print('{}'.format(topN_acc_importance))


    # import ipdb; ipdb.set_trace()
    # plt.plot(np.arange(len(origin_list)), origin_posterior[:len(origin_list)], color='green', label='origin feature', alpha=0.4)
    # plt.plot(np.arange(len(importance_posterior)), importance_posterior[:len(importance_posterior)], color='red', label='importance feature', alpha=0.4)
    # plt.legend() # 显示图例
    # plt.xlabel('waves,\n acc_origin:{},acc_importance:{}'.format(acc_origin,acc_importance))
    # plt.ylabel('target_spk_posterior')
    # pic_path = os.path.dirname(origin)+'posterior.png'
    # plt.savefig(pic_path)


if __name__ == "__main__":
     parser = argparse.ArgumentParser()
     parser.add_argument('--feature', help='file1', type=str, default="")
     parser.add_argument('--saliency', help='file1', type=str, default="")
     parser.add_argument('--option', help='top-N', type=str, default="top-1")

     args = parser.parse_args()
     pic_posterior(args.feature, args.saliency, args.option)
