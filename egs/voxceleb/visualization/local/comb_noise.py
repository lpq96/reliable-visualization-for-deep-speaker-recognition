#!/usr/bin/env python
# encoding: utf-8
import argparse
import os
import random
import tqdm
import ipdb
import sox
# 先  生成一个 noise list
# 遍历  data/multi_a.csv
# 写一个source 文件
# 用sox 把俩语音拼起来

def creat_noise_list(dirName, extension='.wav'):
    if dirName[-1] != os.sep:
        dirName += os.sep
    prefixSize = len(dirName)
    noise_list = []
    print("finding {}, Waiting...".format(extension))
    for root, dirs, filenames in tqdm.tqdm(os.walk(dirName, followlinks=True)):
        filtered_files = [f for f in filenames if f.endswith(extension)]
        if len(filtered_files) > 0:
            for filename in filtered_files:
                full_path = os.path.join(root, filename)
                noise_list.append((full_path))

    print("find {} utterance".format(len(noise_list)))
    return noise_list



def concatenate_wav(data_dir, noise_list):
    target_wav_list_path = os.path.join(data_dir,'data/multi_a.csv')
    os.system('mkdir -p data/data/noise')
    target_wav_list = open(target_wav_list_path,'r')
    lines = target_wav_list.readlines()
    combined_list = []
    combined_source = []
    for line in tqdm.tqdm(lines[1:]):
        spk_label = line.strip().split(',')[-1]
        spk_name = line.split(',')[1]
        source_path = line.split(',')[2]
        noise_wav = random.choice(noise_list)     
        wav_duration = sox.file_info.info(noise_wav)['duration']
        while wav_duration > 20:
          noise_wav = random.choice(noise_list) 
          wav_duration = sox.file_info.info(noise_wav)['duration']
        combined_path = os.path.join('data/data/noise','{}.wav'.format(len(combined_list)))
        os.system("sox --combine concatenate {} {} {}".format(noise_wav, source_path, combined_path))
        combined_list.append((len(combined_list),spk_name,combined_path,spk_label))
        combined_source.append((noise_wav, source_path, combined_path))
        
    test_noise = open('{}/data/test_noise.csv'.format(data_dir),'w')
    test_noise.write(",speaker_name,utt_paths,utt_spk_int_labels\n")
    test_noise_source = open('{}/data/test_noise_source.csv'.format(data_dir),'w')

    for i in range(len(combined_list)):
      test_noise.write("{},{},{},{}\n".format(combined_list[i][0],combined_list[i][1],combined_list[i][2],combined_list[i][3]))
      test_noise_source.write("{},{},{}\n".format(combined_source[i][0],combined_source[i][1],combined_source[i][2]))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='', type=str, default="./data")
    parser.add_argument('--noise_dir', help='', type=str, default="./data")

    args = parser.parse_args()
    noise_list = creat_noise_list(args.noise_dir)
    concatenate_wav(args.data, noise_list)
