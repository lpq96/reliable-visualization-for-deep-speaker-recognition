#!/usr/bin/env python
# encoding: utf-8
import argparse
from tqdm import tqdm
import re
def select_wav(wav_list, vad_list):

    f_source = open(wav_list,'r')
    f_target = open(vad_list,'w')

    lines = f_source.readlines()
    f_target.write(lines[0])
    for line in tqdm(lines[1:]):
      strinfo = re.compile('wav')
      vad_line = strinfo.sub('vad',line)
      f_target.write(vad_line)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--wav_list', help='file1', type=str, default="../data/data/exp1_2_vis_test.csv")
    parser.add_argument('--vad_list', help='file1', type=str, default="../data/data/exp1_2_vis_test_vad.csv")

    args = parser.parse_args()
    select_wav(args.wav_list,args.vad_list)