#!/usr/bin/env python
# encoding: utf-8
import argparse
import os

def concatenate_wav(data_dir):
    target_wav_list_path = os.path.join(data_dir,'data/multi_a.csv')
    source_list_path = os.path.join(data_dir,'data/multi_source_all.csv')

    target_wav_list = open(target_wav_list_path,'r')
    lines = target_wav_list.readlines()
    spk2label = {}
    for line in lines[1:]:
        spk_label = line.strip().split(',')[-1]
        spk_name = line.split(',')[1]
        if spk_name not in spk2label:
            spk2label[spk_name] = spk_label
    os.system('mkdir -p data/data/{a_b,a_b_a,a_b_c,b_a_b}')
    source_list = open(source_list_path, 'r')
    lines = source_list.readlines()
    a_b_wav_list,a_b_a_wav_list,a_b_c_wav_list,b_a_b_wav_list = [],[],[],[]
    for line in lines:
      comb = line.split(',')[-1].strip()

      if 'a_b' in comb and 'b_a_b' not in comb and 'a_b_c' not in comb and 'a_b_a' not in comb:
        a_ba = line.split(',')[0].strip()
        a_bb = line.split(',')[1].strip()
        target_wav_spk_name = line.split(',')[0].split('/')[-3]
        spk_label = spk2label[target_wav_spk_name]
        os.system("sox --combine concatenate {} {} {}".format(a_ba, a_bb, comb))
        a_b_wav_list.append((len(a_b_wav_list),target_wav_spk_name,comb,spk_label))
      if 'a_b_a' in comb:
        a_b_aa1 = line.split(',')[0].strip()
        a_b_ab = line.split(',')[1].strip()
        a_b_aa2 = line.split(',')[2].strip()
        target_wav_spk_name = line.split(',')[0].split('/')[-3]
        spk_label = spk2label[target_wav_spk_name]
        os.system("sox {} {} {} -t wav {}".format(a_b_aa1, a_b_ab, a_b_aa2, comb))
        a_b_a_wav_list.append((len(a_b_a_wav_list),target_wav_spk_name,comb,spk_label))
      if 'a_b_c' in comb:
        a_b_ca = line.split(',')[0].strip()
        a_b_cb = line.split(',')[1].strip()
        a_b_cc = line.split(',')[2].strip()
        target_wav_spk_name = line.split(',')[0].split('/')[-3]
        spk_label = spk2label[target_wav_spk_name]
        os.system("sox {} {} {} -t wav {}".format(a_b_ca, a_b_cb, a_b_cc, comb))
        a_b_c_wav_list.append((len(a_b_c_wav_list),target_wav_spk_name,comb,spk_label))
      if 'b_a_b' in comb:
        b_a_bb1 = line.split(',')[0].strip()
        b_a_ba = line.split(',')[1].strip()
        b_a_bb2 = line.split(',')[2].strip()
        target_wav_spk_name = line.split(',')[1].split('/')[-3]
        spk_label = spk2label[target_wav_spk_name]
        os.system("sox {} {} {} -t wav {}".format(b_a_bb1, b_a_ba, b_a_bb2, comb))
        b_a_b_wav_list.append((len(b_a_b_wav_list),target_wav_spk_name,comb,spk_label))

    test_a_b = open('{}/data/test_a_b.csv'.format(data_dir),'w')
    test_a_b_a = open('{}/data/test_a_b_a.csv'.format(data_dir),'w')
    test_a_b_c = open('{}/data/test_a_b_c.csv'.format(data_dir),'w')
    test_b_a_b = open('{}/data/test_b_a_b.csv'.format(data_dir),'w')
    test_a_b.write(",speaker_name,utt_paths,utt_spk_int_labels\n")
    test_a_b_a.write(",speaker_name,utt_paths,utt_spk_int_labels\n")
    test_a_b_c.write(",speaker_name,utt_paths,utt_spk_int_labels\n")
    test_b_a_b.write(",speaker_name,utt_paths,utt_spk_int_labels\n")

    for i in range(len(a_b_wav_list)):
      test_a_b.write("{},{},{},{}\n".format(a_b_wav_list[i][0],a_b_wav_list[i][1],a_b_wav_list[i][2],a_b_wav_list[i][3]))
      test_a_b_a.write("{},{},{},{}\n".format(a_b_a_wav_list[i][0],a_b_a_wav_list[i][1],a_b_a_wav_list[i][2],a_b_a_wav_list[i][3]))
      test_a_b_c.write("{},{},{},{}\n".format(a_b_c_wav_list[i][0],a_b_c_wav_list[i][1],a_b_c_wav_list[i][2],a_b_c_wav_list[i][3]))
      test_b_a_b.write("{},{},{},{}\n".format(b_a_b_wav_list[i][0],b_a_b_wav_list[i][1],b_a_b_wav_list[i][2],b_a_b_wav_list[i][3]))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', help='', type=str, default="./data")
    args = parser.parse_args()
    concatenate_wav(args.data)