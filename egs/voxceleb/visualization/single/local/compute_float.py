#!/usr/bin/env python
# encoding: utf-8
import argparse
def compute_float(int):
  print(float(int/100))
  return float(int/100)

if __name__ == "__main__":
     parser = argparse.ArgumentParser()
     parser.add_argument('--threshold', help='', type=int, default="")
     args = parser.parse_args()
     compute_float(args.threshold)