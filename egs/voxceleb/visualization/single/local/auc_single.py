#!/usr/bin/env python
# encoding: utf-8
from tkinter import font
import numpy as np
import argparse
import matplotlib.pyplot as plt


def auc(arr):
    """Returns normalized Area Under Curve of the array."""
    return (arr.sum() - arr[0] / 2 - arr[-1] / 2) / (arr.shape[0] - 1)

def pic_auc_map_from_multi_method_new(insertion_file, deletion_file ,deletion_time_align,insertion_time_align , deletion_random, insertion_random):
  
    insertion_file = open(insertion_file,'r')
    acc_list = []
    for line in insertion_file.readlines()[1:]:
        line = line.strip()
        if 'layer' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)

    for i in range(len(acc_list)):
        if 'GradCAMpp,layer4' == acc_list[i]:
          GradCAMpp_layer4_preserve = acc_list[i+1:i+12]
        
        if 'LayerCAM,layer4' == acc_list[i]:
          LayerCAM_layer4_preserve = acc_list[i+1:i+12]
        
        if 'ScoreCAM,layer4' == acc_list[i]:
          ScoreCAM_layer4_preserve = acc_list[i+1:i+12]

    insertion_time_align = open(insertion_time_align,'r')
    acc_list = []
    for line in insertion_time_align.readlines()[1:]:
        line = line.strip()
        if 'time' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)
    for i in range(len(acc_list)):
        if 'time' == acc_list[i]:
          time_aling_insertion = acc_list[i+1:i+12]

    insertion_random = open(insertion_random,'r')
    acc_list = []
    for line in insertion_random.readlines()[1:]:
        line = line.strip()
        if 'random' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)
    for i in range(len(acc_list)):
        if 'random' == acc_list[i]:
          random_insertion = acc_list[i+1:i+12]

    deletion_file = open(deletion_file,'r')
    acc_list = []
    for line in deletion_file.readlines()[1:]:
        line = line.strip()
        if 'layer' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)

    for i in range(len(acc_list)):
        if 'GradCAMpp,layer4' == acc_list[i]:
          GradCAMpp_layer4_occlusion = acc_list[i+1:i+12]
        
        if 'LayerCAM,layer4' == acc_list[i]:
          LayerCAM_layer4_occlusion = acc_list[i+1:i+12]
        
        if 'ScoreCAM,layer4' == acc_list[i]:
          ScoreCAM_layer4_occlusion = acc_list[i+1:i+12]
    
    
    deletion_time_align = open(deletion_time_align,'r')
    acc_list = []
    for line in deletion_time_align.readlines()[1:]:
        line = line.strip()
        if 'time' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)
    for i in range(len(acc_list)):
        if 'time' == acc_list[i]:
          time_aling_occlusion = acc_list[i+1:i+12]
        
    deletion_random = open(deletion_random,'r')
    acc_list = []
    for line in deletion_random.readlines()[1:]:
        line = line.strip()
        if 'random' not in line :
            acc_list.append(float(line))
        else:
            acc_list.append(line)
    for i in range(len(acc_list)):
        if 'random' == acc_list[i]:
          random_occlusion = acc_list[i+1:i+12]
        

    plt.figure(figsize=(28, 10))
    plt.subplot(1,2,2)
    title = 'Insertion Curve'
    h_layer1 = auc(np.array(GradCAMpp_layer4_preserve))
    h_layer2 = auc(np.array(LayerCAM_layer4_preserve))
    h_layer3 = auc(np.array(ScoreCAM_layer4_preserve))
    h_layer4 = auc(np.array(time_aling_insertion))
    h_layer5 = auc(np.array(random_insertion))


    GradCAMpp_layer4_preserve = np.array(GradCAMpp_layer4_preserve)*100
    LayerCAM_layer4_preserve = np.array(LayerCAM_layer4_preserve)*100
    ScoreCAM_layer4_preserve = np.array(ScoreCAM_layer4_preserve)*100
    time_aling_insertion = np.array(time_aling_insertion)*100
    random_insertion = np.array(random_insertion)*100


    # plt.figure(figsize=(5, 5))
    plt.xlabel("Insertion proportion (%)",{'size':36})
    # plt.ylabel("Top-1 Acc (%)",{'size':36})
    plt.xticks(fontsize=32)
    plt.yticks(fontsize=32)
    plt.plot(np.arange(11), GradCAMpp_layer4_preserve[:11],color='blue',label='Grad-CAM++ AUC: {:.2f}'.format(h_layer1),linewidth=5)
    plt.plot(np.arange(11), ScoreCAM_layer4_preserve[:11],color='red',label='Score-CAM AUC: {:.2f}'.format(h_layer3),linewidth=5)
    plt.plot(np.arange(11), LayerCAM_layer4_preserve[:11],color='green',label='Layer-CAM AUC: {:.2f}'.format(h_layer2),linewidth=5)
    plt.plot(np.arange(11), time_aling_insertion[:11],color='black',label='Time-Aligned AUC: {:.2}'.format(h_layer4),linewidth=5,linestyle='--')
    plt.plot(np.arange(11), random_insertion[:11],color='deeppink',label='Random AUC: {:.2}'.format(h_layer5),linewidth=5,linestyle=':')

    plt.legend(loc='upper left', prop = {'size':25})
    # plt.fill_between(np.arange(11), 0, scores[:11], alpha=0.4)
    plt.title("{}".format(title),{'size':42})

    plt.subplot(1,2,1)
    title = 'Deletion Curve'
    h_layer1 = auc(np.array(GradCAMpp_layer4_occlusion))
    h_layer2 = auc(np.array(LayerCAM_layer4_occlusion))
    h_layer3 = auc(np.array(ScoreCAM_layer4_occlusion))
    h_layer4 = auc(np.array(time_aling_occlusion))
    h_layer5 = auc(np.array(random_occlusion))

    GradCAMpp_layer4_occlusion = np.array(GradCAMpp_layer4_occlusion)*100
    LayerCAM_layer4_occlusion = np.array(LayerCAM_layer4_occlusion)*100
    ScoreCAM_layer4_occlusion = np.array(ScoreCAM_layer4_occlusion)*100
    time_aling_occlusion = np.array(time_aling_occlusion)*100
    random_occlusion = np.array(random_occlusion)*100

    plt.xlabel("Deletion proportion (%)",{'size':36})
    plt.ylabel("Top-1 Acc (%)",{'size':36})
    plt.xticks(fontsize=32)
    plt.yticks(fontsize=32)
    plt.plot(np.arange(11), GradCAMpp_layer4_occlusion[:11],color='blue',label='Grad-CAM++ AUC: {:.2f}'.format(h_layer1),linewidth=5)
    plt.plot(np.arange(11), ScoreCAM_layer4_occlusion[:11],color='red',label='Score-CAM AUC: {:.2f}'.format(h_layer3),linewidth=5)
    plt.plot(np.arange(11), LayerCAM_layer4_occlusion[:11],color='green',label='Layer-CAM AUC: {:.2f}'.format(h_layer2),linewidth=5)
    plt.plot(np.arange(11), time_aling_occlusion[:11],color='black',label='Time-Aligned AUC: {:.2}'.format(h_layer4),linewidth=5,linestyle='--')
    plt.plot(np.arange(11), random_occlusion[:11],color='deeppink',label='Random AUC: {:.2}'.format(h_layer5),linewidth=5,linestyle=':')
    plt.legend(loc='upper right',prop = {'size':25})
    # plt.fill_between(np.arange(11), 0, scores[:11], alpha=0.4)
    plt.title("{}".format(title),{'size':42})
    plt.savefig('AUC.png')
    plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--deletion_file', help='file1', type=str, default="./deletion_vad_single_speaker.log")
    parser.add_argument('--insertion_file', help='file1', type=str, default="./insertion_vad_single_speaker.log")
    parser.add_argument('--deletion_time_align', help='file1', type=str, default="./deletion_time_vad_single_speaker.log")
    parser.add_argument('--insertion_time_align', help='file1', type=str, default="./insertion_time_vad_single_speaker.log")
    parser.add_argument('--deletion_random', help='file1', type=str, default="./deletion_random_vad_single_speaker.log")
    parser.add_argument('--insertion_random', help='file1', type=str, default="./insertion_random_vad_single_speaker.log")
    args = parser.parse_args()
    pic_auc_map_from_multi_method_new(args.insertion_file, args.deletion_file, args.deletion_time_align, args.insertion_time_align , args.deletion_random, args.insertion_random)

