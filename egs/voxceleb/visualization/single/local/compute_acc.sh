#!/bin/bash
stage=$1
mkdir -p result

if [ $stage -eq 1 ];then
# ! single-speaker auc by saliency map
for type in insertion deletion 
do
result=result/${type}_vad_single_speaker.log
time=$(date "+%Y-%m-%d %H:%M:%S")
echo $time > $result
for visualization_type in GradCAMpp ScoreCAM LayerCAM
  do
  for layer in layer4
  do
  echo $visualization_type,$layer >> $result
  for threshold in 0 10 20 30 40 50 60 70 80 90 100
  do
    i=`python3 local/compute_float.py --threshold $threshold`
    python3 local/pic_posterior_acc.py \
    --feature  salient_np_vad/${visualization_type}/${layer}/${visualization_type}_origin_scores_save_acc_${type}_${i}.csv \
    --saliency  salient_np_vad/${visualization_type}/${layer}/${visualization_type}_salient_scores_save_acc_${type}_${i}.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
  done
  done
done

# ! single-speaekr auc by baseline(time-aligned)
for type in insertion deletion 
do
result=result/${type}_time_vad_single_speaker.log
time=$(date "+%Y-%m-%d %H:%M:%S")
echo $time >$result
for visualization_type in _
  do
  echo time >> $result
  for threshold in 0 10 20 30 40 50 60 70 80 90 100
  do
    i=`python3 local/compute_float.py --threshold $threshold`
    python3 local/pic_posterior_acc.py \
    --feature  salient_np_vad/baseline/${visualization_type}_origin_scores_save_acc_time_${type}_${i}.csv \
    --saliency  salient_np_vad/baseline/${visualization_type}_salient_scores_save_acc_time_${type}_${i}.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
  done
done

# ! single-speaekr auc by baseline(random)
for type in insertion deletion 
do
result=result/${type}_random_vad_single_speaker.log
time=$(date "+%Y-%m-%d %H:%M:%S")
echo $time > $result
for visualization_type in _
  do
  echo random >> $result
  for threshold in 0 10 20 30 40 50 60 70 80 90 100
  do
    i=`python3 local/compute_float.py --threshold $threshold`
    python3 local/pic_posterior_acc.py \
    --feature  salient_np_vad/baseline/${visualization_type}_origin_scores_save_acc_random_${type}_${i}.csv \
    --saliency  salient_np_vad/baseline/${visualization_type}_salient_scores_save_acc_random_${type}_${i}.csv \
    --option top-1 \
    2>&1 | tee -a $result || exit 1
  done
  done
done
fi

